import React from "react";
import ButtonStyle from './Button.module.scss';

export default function Button(props) {
    return ( 
        <button className={ButtonStyle.button} style={{backgroundColor: props.backgroundColor}} onClick={props.onClick}>
            {props.text}
        </button>
     );
}
