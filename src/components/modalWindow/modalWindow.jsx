import React from "react";
import style from "./modalWindow.module.scss"

const ModalWindow = (props) => {
    return(
        <div className={style.window} onClick={(e)=>{
            if (e.target.className.includes('window'))
            {
                props.closeModal()
            }
        }}>
            <div className={style.content} style={{backgroundColor: props.backgroundColor}}>
            {props.closeButton && <span 
                                className={style.closeBtn}
                                onClick={props.closeModal}
                                >&#128937;
                            </span>}
                <p className={style.header}>{props.header}</p>
                <p className={style.text}>{props.text}</p>
                <div className={style.buttonWrapper}>{props.actions}</div>
            </div>
        </div>
    );
}

export default ModalWindow;