import React, { useState } from "react";
import Button from "./components/button/Button";
import style from "./app.module.scss"
import ModalWindow from "./components/modalWindow/modalWindow";

function App(){

    const [isOpenModalFirst, setisOpenModalFirst] = useState(false);
    const [isOpenModalSecond, setisOpenModalSecond] = useState(false);

    function openModalFirst(){
        setisOpenModalFirst(true);
    }
    function closeModalFirst(){
        setisOpenModalFirst(false);
    }
    function openModalSecond(){
        setisOpenModalSecond(true);
    }
    function closeModalSecond(){
        setisOpenModalSecond(false);
    }

    return(
        <>
            {isOpenModalFirst && <ModalWindow 
            header='Delete file' 
            text='Are you sure? All files will be delete!' 
            closeButton={true} 
            closeModal={closeModalFirst}
            backgroundColor='#ff4242'
            actions={
                <>
                    <button className={style.buttonForDeleteModal} onClick={closeModalFirst}>Ok</button>
                    <button className={style.buttonForDeleteModal} onClick={closeModalFirst}>Cancel</button></>}
                />
            }
            {isOpenModalSecond && <ModalWindow 
            header='Add file' 
            text='Are you sure? You will be add file to database!' 
            closeButton={false} 
            closeModal={closeModalSecond}
            backgroundColor='#3e9657'
            actions={
                <>
                    <button className={style.buttonForDeleteModal} onClick={closeModalSecond}>Ok</button>
                    <button className={style.buttonForDeleteModal} onClick={closeModalSecond}>Cancel</button></>}
                />
            }
            <div className={style.btnContainer}>
                <Button text={'Open first modal'} backgroundColor={'#ffffff'} onClick={openModalFirst}/>
                <Button text={'Open second modal'} backgroundColor={'#fcce12'} onClick={openModalSecond}/>
            </div>
        </>
    );
}

export default App;